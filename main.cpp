#include <iostream>
#include <limits>
#include <tuple>

#define POW(a) ((a) * (a))
#define MAX(a, b) ((a) > (b) ? (a) : (b))

namespace geometry
{
	/*TRAITS*/
	namespace traits
	{
		template <typename T, std::size_t N>
		struct access
		{
			static constexpr double get(const T&)
			{ return 0.0; }
		};

		template <std::size_t N> struct num
		{ static constexpr std::size_t value = N; };

		template <typename T>
		struct dimension : num<3> {};
	}



	/*GET A VALUE*/
	template <std::size_t N, typename T, bool B>
	struct distribute
	{
		static constexpr double get(const T & p)
		{ return traits::access<T, N>::get(p); }
	};

	template <std::size_t N, typename T>
	struct distribute<N, T, false>
	{
		static constexpr double get(const T&)
		{ return 0.0; }
	};

	template <std::size_t N, typename T>
	constexpr double getV(const T & p = T())
	{ return distribute<N, T, traits::dimension<T>::value - 1 >= N>::get(p); }



	/*COMPUTE*/
	template <typename T1, typename T2, std::size_t N>
	struct computation
	{
		static constexpr double compute(const T1 & p1, const T2 & p2)
		{
			return POW(getV<N - 1>(p1) - getV<N - 1>(p2))
			+ computation<T1, T2, N - 1>::compute(p1, p2);
		}
	};

	template <typename T1, typename T2>
	struct computation<T1, T2, 0>
	{
		static constexpr double compute(const T1&, const T2&)
		{ return 0.0; }
	};



	/*CT_SQRT*/
	constexpr double sqrtNewtonRaphson(double x, double curr, double prev)
	{
		return curr == prev ?
			curr : sqrtNewtonRaphson(x, 0.5 * (curr + x / curr), curr);
	}

	constexpr double ct_sqrt(double x)
	{
		return x >= 0 && x < std::numeric_limits<double>::infinity() ?
			sqrtNewtonRaphson(x, x, 0) : std::numeric_limits<double>::quiet_NaN();
	}



	/*DISTANCE*/
	template <typename T1, typename T2>
	constexpr double getDistance(const T1 & p1 = T1(), const T2 & p2 = T2())
	{
		return ct_sqrt(computation<T1, T2,
			MAX(traits::dimension<T1>::value, 
				traits::dimension<T2>::value)>::compute(p1, p2));
	}
}

struct Point
{ double x, y, z; };

namespace geometry
{
	namespace traits
	{
		template <> struct dimension<Point> : num<3> {};

		template <> struct access<Point, 0>
		{
			static constexpr double get(const Point & p)
			{ return p.x; }
		};

		template <> struct access<Point, 1>
		{
			static constexpr double get(const Point & p)
			{ return p.y; }
		};

		template <> struct access<Point, 2>
		{
			static constexpr double get(const Point & p)
			{ return p.z; }
		};
	}
}

namespace geometry
{
	namespace traits
	{
		template <typename... A>
		struct dimension<std::tuple<A...>>
		: num<std::tuple_size<std::tuple<A...>>::value> {};

		template <std::size_t N, typename... A>
		struct access<std::tuple<A...>, N>
		{
			static constexpr double get(const std::tuple<A...> & p)
			{ return std::get<N>(p); }
		};
	}
}

int main(int argc, char ** argv) 
{
	constexpr std::tuple<int, float> p1(5, 0.3);
	constexpr Point p2 = { 8.5, 3, 7.1 };

	constexpr double distance = geometry::getDistance(p1, p2);

	std::cout << "Distance: " << distance << std::endl;

	system("pause");

	return 0;
}